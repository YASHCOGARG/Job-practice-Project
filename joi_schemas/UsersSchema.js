const Joi = require('joi');


const UserSchema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(8).required()

});

const userGrantPermissionSchema = Joi.object({
    emailID: Joi.string().email().required(),
});

const UpdateUserSchema = Joi.object({
    name: Joi.string().optional(),
    email: Joi.string().email().optional(),
    password: Joi.string().min(8).optional()

});

module.exports = { UserSchema, userGrantPermissionSchema,UpdateUserSchema }