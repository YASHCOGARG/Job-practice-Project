let express = require('express')
let app = express();
const mongoose = require('mongoose')
const env = require('dotenv')
const initalizeAdminUser = require('./db/initalizeAdminUser')

// ROUTES 
const authRoute = require('./routes/register')
const loginRoute = require('./routes/login')

// CONFIGURATION
env.config();

// Connect TO DB 
mongoose.connect(process.env.MONGO_DB_URL, () => {
    console.log("CONNECTED TO MONGO DB")
});
initalizeAdminUser();

// MIddle-Wares
app.use(express.json())

// Routes Midd
app.use('/api/user', authRoute);
app.use('/api/user', loginRoute);

const PORT = (process.env.PORT || 3001);
app.listen(PORT, () => console.log(`Server is running on ${PORT}`))