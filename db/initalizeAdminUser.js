const User = require('../models/User')
const bcryptjs = require('bcryptjs')


module.exports = async function () {

    try {
        const user = new User({
            name: "admin",
            email: "admin@admin.com",
            password: "123456789",
            role: "admin"
        })

        const salt = await bcryptjs.genSalt(10);
        const hashpassword = await bcryptjs.hash(user.password, salt);
        user.password = hashpassword;
        const adminUserPresent = await User.findOne({ email: user.email });

        if (!adminUserPresent) {
            const saveUser = await user.save();
            console.log("Admin User Created")
        }
    }
    catch (err) {
        console.log("Failed to create an intial admin User" + err)
    }

}