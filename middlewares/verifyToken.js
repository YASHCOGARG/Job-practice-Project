const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    try {
        const token = req.header('auth-token');
        if (!token) {
            return res.status(401).send('Access Denied')
        }
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user = verified;
        next();
    }
    catch (err) {
        throw {
            status: 401,
            message: 'Failed to verify the token '
        }
    }
}