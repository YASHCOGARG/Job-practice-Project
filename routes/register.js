const router = require('express').Router();
const User = require('../models/User')
const { UserSchema, userGrantPermissionSchema, UpdateUserSchema } = require('../joi_schemas/UsersSchema')
const bcryptjs = require('bcryptjs')
const verify = require('../middlewares/verifyToken')
const jwt = require('jsonwebtoken')
/**
 * ADD USER 
 */
router.post('/addUser', verify, async (req, res) => {
    let token = req.headers['auth-token'];
    const verified = jwt.verify(token, process.env.TOKEN_SECRET)
    req.user = verified;
    try {

        const checkAuthorization = await User.findOne({ _id: req.user });

        if (checkAuthorization.role != "admin") {
            throw {
                status: 401,
                message: "User Not Authorized to add User"
            }
        }
    }
    catch (err) {

        res.status(401).send(err)
        return
    }
    try {


        const { value, error } = UserSchema.validate(req.body);

        if (error != null) {
            res.status(400).send(error.details[0].message);
        }
        else {
            const checkemail = await User.findOne({ email: req.body.email });
            if (checkemail) {
                throw {
                    error: 400,
                    message: 'Email Exists already'
                }
            }

            const salt = await bcryptjs.genSalt(10);
            const hashpassword = await bcryptjs.hash(req.body.password, salt);


            const user = new User({
                name: req.body.name,
                email: req.body.email,
                password: hashpassword
            })


            try {
                const saveUser = await user.save();
                const { name, email, _id, role } = saveUser;
                res.send({ name, email, _id, role });
            }
            catch (err) {
                res.status(400).send("FAiled to register the user " + err);
            }
        }
    }
    catch (err) {
        res.status(400).send(err);
    }


});
/**
 * GET USER BY ID 
 */
router.get('/getUser', verify, async (req, res) => {

    try {
        if (req.query.emailID) {

            const user = await User.findOne({ email: req.query.emailID });
            if (user == null) {
                res.send({});
                return;
            }
            const { _id, name, email, role } = user

            console.log(user)
            res.send({ _id, name, email, role })
        }
        else {
            throw {
                status: 400,
                message: "Please Populate the emailID"
            }
        }
    }
    catch (err) {
        res.status(400).send(err);
        return;
    }


});
/**
 * DELETE USER BY ID 
 */
router.delete('/deleteUser', verify, async (req, res) => {
    try {

        if (req.query.emailID) {

            const user = await User.deleteOne({ email: req.query.emailID });
            if (user == null) {
                res.send({});
                return;
            }

            delete user.password;
            res.send(user)
        }
        else {
            throw {
                status: 400,
                message: "Please Populate the emailID"
            }
        }
    }
    catch (err) {
        res.status(400).send(err);
        return;
    }
});
/**
 * Update User By ID 
 */
router.put('/updateUser', verify, async (req, res) => {
    try {
        if (req.query.emailID) {
            if (req.body.role) {
                delete req.body.role
            }
            if (req.body._id) {
                delete req.body._id
            }
            if (req.body.password) {
                const salt = await bcryptjs.genSalt(10);
                const hashpassword = await bcryptjs.hash(req.body.password, salt);
                req.body.password = hashpassword;

            }
            const { value, error } = await UpdateUserSchema.validate(req.body);
            if (error) {
                throw {
                    status: 400,
                    message: error
                }
            }

            const user = await User.updateOne({ email: req.query.emailID }, { ...req.body });
            if (user == null) {
                res.send({});
                return;
            }

            delete user.password;
            res.send(user)
        }
        else {
            throw {
                status: 400,
                message: "Please Populate the emailID"
            }
        }
    }
    catch (err) {
        res.status(400).send(err);
        return;
    }
});
/**
 * Grant Permission TO User 
 */
router.post('/grantUserPermission', verify, async (req, res) => {
    let token = req.headers['auth-token'];
    const verified = jwt.verify(token, process.env.TOKEN_SECRET)
    req.user = verified;
    try {
        const { value, error } = await userGrantPermissionSchema.validate(req.body);
        if (error) {
            throw {
                status: 400,
                message: "Bad Request emailID is required"
            }
        }
        const checkAuthorization = await User.findOne({ _id: req.user });

        if (checkAuthorization.role != "admin") {
            throw {
                status: 401,
                message: "User Not Authorized to grant Permission to  User only admin can do this"
            }
        }
        const user = await User.updateOne({ email: req.body.emailID }, { role: "admin" });
        if (user == null) {
            res.send({});
        }
        else {
            res.send(user);
        }
    }
    catch (err) {

        res.status(400).send(err)
        return
    }
})

module.exports = router;
