const router = require('express').Router();
const User = require('../models/User')
const { UserSchema } = require('../joi_schemas/UsersSchema')
const loginSchema = require('../joi_schemas/loginSchema.js')
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')


router.post('/login', async (req, res) => {
    try {
        const { value, error } = loginSchema.validate(req.body);
        if (error) {
            throw {
                error: 400,
                message: 'Email or Password is Missing'
            }
        }
        const { email } = req.body;
        const user = await User.findOne({ email: email });
        if (!user) {
            throw {
                error: 400,
                message: 'Email or Password is Incorrect'
            }
        }
        const validPassword = await bcryptjs.compare(req.body.password, user.password)
        if (!validPassword) {
            throw {
                error: 400,
                message: 'Email or Password is Incorrect'
            }
        }
        else {
            const token = jwt.sign({ _id: user.id ,role : user.role,email:user.email}, process.env.TOKEN_SECRET,{ expiresIn: '1h' });
            res.send({ "jwttoken": token });
        }

    }
    catch (err) {
        res.status(400).send(err);
    }


});

module.exports = router;